<?php

namespace Nana\Cqrs;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Nana\Cqrs\Console\Commands\CommandHandlerMakeCommand;
use Nana\Cqrs\Console\Commands\CommandMakeCommand;
use Nana\Cqrs\Console\Commands\InitProjectCommand;
use Nana\Cqrs\Console\Commands\ModelMakeCommand;
use Nana\Cqrs\Console\Commands\ModuleMakeCommand;
use Nana\Cqrs\Console\Commands\QueryHandlerMakeCommand;
use Nana\Cqrs\Console\Commands\QueryMakeCommand;
use Nana\Cqrs\Console\Commands\RepositoryInterfaceMakeCommand;
use Nana\Cqrs\Console\Commands\RepositoryMakeCommand;
use Nana\Cqrs\Console\Commands\TransformerMakeCommand;
use Nana\Cqrs\Bus\CommandBus;
use Nana\Cqrs\Bus\QueryExecutor;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CommandHandlerMakeCommand::class,
                CommandMakeCommand::class,
                InitProjectCommand::class,
                ModelMakeCommand::class,
                ModuleMakeCommand::class,
                QueryHandlerMakeCommand::class,
                QueryMakeCommand::class,
                RepositoryInterfaceMakeCommand::class,
                RepositoryMakeCommand::class,
                TransformerMakeCommand::class,
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $commandNamespace = "Command";
        $commandHandlerNamespace = "Command\Handlers";

        $this->app->singleton(CommandBus::class, function ($app) use ($commandNamespace, $commandHandlerNamespace){
            return new CommandBus($app, $commandNamespace, $commandHandlerNamespace);
        });

        $queryNamespace = "Query";
        $queryHandlerNamespace = "Query\Handlers";

        $this->app->singleton(QueryExecutor::class, function ($app) use ($queryNamespace, $queryHandlerNamespace){
            return new QueryExecutor($app, $queryNamespace, $queryHandlerNamespace);
        });
    }
}
